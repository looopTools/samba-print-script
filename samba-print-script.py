#!/usr/bin/env python

"""
BSD 3-Clause License

Copyright (c) 2020, Lars Nielsen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import subprocess
import sys
import os
import argparse
import json

import shlex

# Change to where you put the config file
configFilePath = 'print_config.json'

def getServerAndDomain():
    with open(configFilePath, 'r') as configFile:
        data = json.loads(configFile.read())
        return data['server'], data['domain']

def printDoc(printer, user, file):
    server = getServer()
    res = subprocess.check_call(['smbclient', '{!s}{!s}'.format(server, printer),
                                 '-U', user, '-W', 'uni', '-c', 'print ' + shlex.quote(file)],
                                encoding='utf-8')


def convertToDuplex(file):
    psFile = '{!s}.ps'.format(os.path.splitext(file)[0])

    subprocess.call(['pdftops', '-paper', 'A4',
                     '-duplex', file, psFile], encoding='utf-8')
    return psFile


def listPrinters():
    with open(configFilePath, 'r') as configFile:
        data = json.loads(configFile.read())
        printers = data['printers']

        for index, printer in enumerate(printers):
            print("{!s}: {!s}".format(index, printer))

def getPrinter(i=0):
    with open(configFilePath, 'r') as configFile:
        data = json.loads(configFile.read())
        printers = data['printers']
        return printers[i]['name']
        

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--printer', '-p', help='set the printer')
    parser.add_argument('--document', help='set the documnet')
    parser.add_argument('--user', '-u', help='set the user')
    parser.add_argument('--duplex', '-d', help='set the user', action='store_true')
    parser.add_argument('--printers', help='print list of all printers', action='store_true')
    args = parser.parse_args()

    if args.printers:
        listPrinters()
        sys.exit(1)

    if not args.document:
        print('You have not provided a document to print')
        sys.exit(1)

    if not args.user: 
        print("You need to provided a user")
        sys.exit(1)        
           
    document = args.document


    if args.duplex:
        document = convertToDuplex(document)


    printer = ''
    if args.printer:
        printer = getPrinter(int(args.printer))
    else:
        printer = getPrinter()

    printDoc(printer, args.user, document)

    if args.duplex:
        subprocess.call(['rm', document], encoding='utf-8')

    
    
