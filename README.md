# Print over SAMBA script 

This script allows you to print documents through printers connected by a SAMBA server using the command line on Linux. 
The script is written in Python and requires Python 3.6 or above. 
Additionally it requires that you have the `smbclient` and `pdftops` tools installed as well. 

A configuration file is required. 
The file must be in JSON format and have the following structure: 

```json
{
    "server": "SERVER_URI",
    "domain": "WORKGROUP_DOMAIN", 
    "printers": [
        {
            "name": "PRINTER_NAME", 
            "location": "WHERE IS THIS THING"
        }
    ]
}
```

For the script to work on your machine, you have to change [line 44](https://gitlab.com/looopTools/samba-print-script/-/blob/master/samba-print-script.py#L44) of the script to reflect the placement of your configuration file.


